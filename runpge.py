#!/usr/bin/python3
from pypge.search import PGE
from pypge import expand
from pypge import fitness_funcs as FF
import yaml
from sys import argv
from time import time

import numpy as np
from pandas import read_csv
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d

## run PGE on data
print("Running PGE...")
filename=argv[1]
resultfile=argv[2]
dataframe=read_csv(filename, sep=',',header=0)
cols=dataframe.columns[:-1]
target=dataframe.columns[-1]
cf = open('conf_final.yml', 'r')
config = yaml.load(cf)
pge = PGE(
	search_vars = target,
	usable_vars = cols,
	log_dir='pgelogs/',

	**config
)
ins = dataframe[cols].as_matrix().T+100.0
outs = dataframe[target].values
start = time()
pge.fit(ins,outs)
time_pge = time() - start
best=pge.models[sorted([(m.score, i) for i,m in enumerate(pge.models) if m.score is not None])[0][1]]

ypge = best.predict(best, pge.vars, ins)
np.savetxt(resultfile, ypge, delimiter=",")
timef = open('pge_time', 'w')
timef.write(str(time_pge))
