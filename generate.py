#!/usr/bin/python2.7

from modelgen import *
import ffx
import numpy as np
from random import seed

## generate 2D functions and sample data
print('--- 2D Functions: ---')
regular_2D_samples = []
irregular_2D_samples = []
for i in range(1,21):
	seed(i)
	f = modelgen(8, 5, 1, constreplace = True, varnames=["x"])
	print "2D_{0:02d}: f(x) = ".format(i) + f.pr()
	regular_2D_samples += [data(sample_exp(f, 200, regular=True),
			f,'2D_{0:02d}_regular'.format(i))]
	irregular_2D_samples += [data(sample_exp(f, 200, regular=False),
			f,'2D_{0:02d}_irregular'.format(i))]

## generate 3D functions and sample data
print('--- 3D Functions: ---')
regular_3D_samples = []
irregular_3D_samples = []
for i in range(1,21):
	seed(i)
	f = modelgen(6, 4, 2, constreplace = True, varnames=["x","y"])
	print "3D_{0:02d}: f(x, y) = ".format(i) + f.pr()
	regular_3D_samples += [data(sample_exp(f, 1600, regular=True),
			f,'3D_{0:02d}_regular'.format(i))]
	irregular_3D_samples += [data(sample_exp(f, 1600, regular=False),
			f,'3D_{0:02d}_irregular'.format(i))]

## write data to files
for result in regular_2D_samples+irregular_2D_samples+regular_3D_samples+irregular_3D_samples:
	result.writetocsv('samples/' + result.name + '.csv', True)
	
