\documentclass[10pt, handout]{beamer}
\usepackage{graphics}
\usepackage{tikz}
\usepackage[nocenter]{qtree}

\begin{document}
\title{Examining Various Symbolic Regression Algorithms as Tools for Function and Curve Fitting}
\author{Wanja Hentze} 
\date{\includegraphics[height=2cm]{figures/logo_stce}}

\begin{frame}[plain]
\titlepage
\end{frame}

\begin{frame}{Overview}
\tableofcontents
\end{frame}

\begin{frame}[fragile]
\frametitle{\insertframenumber~Recap: The Function Fitting Problem}
\begin{itemize}[<+->]
\item \textbf{Curve Fitting}: Given $n$ data points $(x_n, y_n)$, find a function $f$ that minimizes $\vert f(x_n) - y_n \vert_*$
\item \textbf{Surface Fitting}: Given $n$ data points $(x_{1_n}, x_{2_n}, y_n)$, find a function $f$ that minimizes $y_{err} = \vert f(x_{1_n}, x_{2_n}) - y_n \vert_*$
\item K-dimensional case: Given $n$ data points $(x_{1_n}, \dots, x_{k_n}, y_n)$, find a function $f$ that minimizes $y_{err} = \vert f(x_{1_n}, \dots, x_{k_n}) - y_n \vert_*$
\item $\vert\cdot\vert_*$ is usually the euclidean norm, making $y_{err}$ the \textbf{Mean Squared Error} or MSE.
\item Extra requirement: Avoid \textbf{Overfitting}.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{\insertframenumber~Recap: Traditional Fitting Models}
\begin{itemize}[<+->]
\item \textbf{Linear Least-Squares fit}: Find $c_1, \dots, c_k, b$ that minimize MSE for $f = c_1x_1 + \dots + c_kx_k + b$.
\begin{itemize}
\item Only yields good fits for simple relationships.
\item Will \textbf{underfit} most data.
\end{itemize}
\item \textbf{Nonlinear Least-Squares fit}: Pick a \textbf{model} $f(x:c)$, then find $c_1, \dots, c_k$ that minimize MSE for $f(x:c)$.
\begin{itemize}
\item Example: $f(x:c) = c_1x^3 + c_2x^2 + c_3x + c_4$ is a \textbf{Polynomial Model}.
\item Can yield good results if the model is chosen appropriately.
\end{itemize}
\item \textbf{Splines}: Fit a piecewise polynomial, smooth function to data.
\begin{itemize}
\item No model needs to be chosen.
\item Decently fits most data.
\item Resulting term for f is very complex.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{\insertframenumber~Recap: Symbolic Regression}
\begin{itemize}[<+->]
\item \textbf{New Idea}: don't choose a model, but make the model part of the optimization itself!
\item Construct candidate models from a base set \textbf{symbolically}.
\item Hopefully, fit many different data sets without manual adjustments.
\item Various algorithms exist (GP-SR, PGE, FFX, \dots).
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{\insertframenumber~GP-SR}
\begin{itemize}[<+->]
\item Symbolic Regression via \textbf{Genetic Programming}
\item Oldest approach by far, first published in 1992.
\item Candidate functions form a "gene pool" and are subjected to natural selection, mutation and crossover.
\item Fitness function for GP is $y_{err}$.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{\insertframenumber~GP-SR: Algorithm}
\begin{enumerate}[<+->]
\item Generate a random initial pool of syntax trees (functions).
\item Evaluate fitness for each function according to the data.
\item Fill the next generation by producing offspring from the fittest functions using the \textbf{mutate} and \textbf{crossover} operations.
\end{enumerate}
\end{frame}

\begin{frame}[fragile]
\frametitle{\insertframenumber~GP-SR: Crossover Operation}
\begin{center}
\Tree[.+ [.$\cdot$ [.cos $x_1$ ]!{\qframesubtree} 3.5 ]
         [.+ [.exp $x_2$ ]
             [.sin [.abs $x_1$ ]]]]~~
\Tree[.* [.+ $x_2$ -1.9 ]
         [.+ [.sin $x_1$ ]
             [.sin [.$\cdot$ 32.4 $x_1$ ] ]!{\qframesubtree} ]]
\\$\downarrow crossover \downarrow$
\\
\Tree[.+ [.$\cdot$ [.sin [.$\cdot$ 32.4 $x_1$ ] ]!{\qframesubtree} 3.5 ]
         [.+ [.exp $x_2$ ]
             [.sin [.abs $x_1$ ]]]]
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{\insertframenumber~GP-SR: Mutation Operation 1}
\begin{center}
\Tree[.+ [.$\cdot$ [.sin [.$\cdot$ 32.4 $x_1$ ] ]3.5 ]
         [.+ [.exp \framebox{$x_2$} ]
             [.sin [.abs $x_1$ ]]]]
\\$\downarrow$ mutation 1 $\downarrow$
\\
\Tree[.+ [.$\cdot$ [.sin [.$\cdot$ 32.4 $x_1$ ] ]3.5 ]
         [.+ [.exp [.sin $x_2$ ]!{\qframesubtree} ]
             [.sin [.abs $x_1$ ]]]]
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{\insertframenumber~GP-SR: Mutation Operation 2}
\begin{center}
\Tree[.+ [.$\cdot$ [.sin [.$\cdot$ 32.4 $x_1$ ] ]3.5 ]
         [.+ [.exp [.sin $x_2$ ]]
             [.sin [.abs $x_1$ ]!{\qframesubtree} ]]]
\\$\downarrow$ mutation 2 $\downarrow$
\\
\Tree[.+ [.$\cdot$ [.sin [.$\cdot$ 32.4 $x_1$ ] ]3.5 ]
         [.+ [.exp [.sin $x_2$ ]]
             [.sin \framebox{$x_1$} ]]]
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{\insertframenumber~GP-SR: Other mutations}
\begin{tabular}{c|c|c}
\Tree[.+ [.sin $x_1$ ] 1.5 ] ~&~ \Tree[.+ [.sin $x_1$ ] 1.5 ] ~&~ \Tree[.+ [.sin $x_1$ ] 1.5 ]\\ & & \\
$\downarrow$ constant swap $\downarrow$ ~&~
$\downarrow$ variable swap $\downarrow$ ~&~
$\downarrow$ operator swap $\downarrow$ \\ & & \\
\Tree[.+ [.sin $x_1$ ] 8.0 ] ~&~ \Tree[.+ [.sin $x_2$ ] 1.5 ] ~&~ \Tree[.$\cdot$ [.sin $x_1$ ] 1.5 ]
\end{tabular}
\end{frame}

\begin{frame}[fragile]
\frametitle{\insertframenumber~GP-SR: Considerations}
A lot of choices are to be made when implementing GP-SR:
\begin{itemize}[<+->]
\item Which operators/base functions to use?
\item Which mutations to use?
\item What range to pick constants from?
\item Probability of mutation/crossover?
\item How to pick subtrees for mutation crossover?
\item Size of candidate pool and number of generations?
\item How to penalize overfitting?
\item \dots
\end{itemize}
$\Rightarrow$ Different GP-SR implementations vary wildly in performance.
\end{frame}

\end{document}
