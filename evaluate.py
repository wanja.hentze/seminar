#!/usr/bin/python2.7

from sys import argv, exit
from os import path
from subprocess import call
from time import time
from datetime import datetime

import ffx
from gplearn import genetic

import numpy as np
from pandas import read_csv
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from scipy import interpolate

if(len(argv) < 2):
	print("Please specify one or more data files to run the benchmark on")
	exit(1)

resultsdir = 'results/'
reportsdir = 'reports/'
timestring = datetime.now().isoformat()
report = open(reportsdir+'report-'+ timestring + '.csv', 'w')
report.write('file, time(gplearn), mse(gplearn), time(pge), mse(pge), time(ffx), mse(ffx), time(spline), mse(spline)\n')

class algorithm():
	def run(self, name, data):
		pass

class scipy(algorithm):
	name = 'spline'
	def run(self, name, data):
		## run scipy interpolations on data
		print("Running scipy.interpolate...")
		data_x = np.array(data[:,:-1])
		data_y = data[:,-1]
		if(data.shape[1] == 2):
			## regular 1-D cubic spline fit
			start = time()
			## only take every 10th point to avoid overfitting
			spline = interpolate.interp1d(data_x[::10,0], data_y[::10],
				kind='cubic')
			self.runtime = time()-start
			self.yhat = spline(data_x[:,0])
		elif '_regular' in name:
			## 2D grid bspline fit
			numx = 1 + min([i for i,x in
				enumerate(data_x[1:,0]) if x > data_x[i-1,0]])
			numy = 1 + min([i for i,x in
				enumerate(data_x[:-1,1]) if x > data_x[i+1,1]])
			## only take every 16th point to avoid overfitting
			X1small = data_x[::4*numy,0]
			X2small = data_x[:numy:4,1]
			Z = np.reshape(data_y, (numx, numy))
			Zsmall = Z[::4,::4]
			start = time()
			bspline = interpolate.RectBivariateSpline(X1small, X2small, Zsmall)
			self.runtime = time()-start
			self.yhat = bspline.ev(data_x[:,0],data_x[:,1])
		else:
			## 2d generic bspline fit
			start = time()
			spline = interpolate.bisplrep(data_x[::16,0],data_x[::16,1],data_y[::16],
				s=1)
			self.runtime = time()-start
			self.yhat = [interpolate.bisplev(x[0], x[1], spline) for x in data_x]
		

class pge(algorithm):
	name = 'pge'
	def run(self, name, data):
		## run PGE (externally because it's Python 3...)
		print("Running PGE...")
		data_x = np.array(data[:,:-1])
		data_y = data[:,-1]
		pge_results = resultsdir+timestring+'_'+name+'_pgetmp.csv'
		call(['./runpge.py', filename, pge_results])
		self.yhat = read_csv(pge_results, sep=',',header=None)[0]
		f = open('pge_time', 'r')
		self.runtime = f.read()


class ffx(algorithm):
	name = 'ffx'
	def run(self, name, data):
		## split data 50/50 into training and testing points for ffx
		training = data[::2]
		testing	= data[1::2]
		train_x = np.array(training[:,:-1])
		train_y = training[:,-1]
		test_x = np.array(testing[:,:-1])
		test_y = testing[:,-1]

		## run ffx on data, using 1/4 as training, the rest as test data
		print("Running ffx...")
		start = time()
		models = ffx.run(train_x, train_y, test_x, test_y, ['x'], verbose=True)
		self.runtime = time() - start
		self.yhat = models[-1].simulate(data[:,-1])

class gp(algorithm):
	name = 'gp'		
	def run(self, name, data):
	## run gplearn SR on data
		print("Running gplearn...")
		gpsr = genetic.SymbolicRegressor(
			#population_size=1000,
			stopping_criteria=0.001*variance,
			comparison=False,
			transformer=True,
			trigonometric=True,
			generations=20,
			#p_crossover=0.7,
			#p_subtree_mutation=0.1,
			#max_samples=0.9,
			verbose=1,
			#parsimony_coefficient=0.0001,
			random_state=0,
			metric='mse',
			n_jobs=-1
			)
		data_x = np.array(data[:,:-1])
		data_y = data[:,-1]
		start = time()
		gpsr.fit(data_x, data_y)
		self.runtime = time() - start
		print("Solution: " + str(gpsr._program))
		self.yhat = gpsr.predict(data_x)
				
## algorithms to be run
algos = [scipy(), pge(), ffx(), gp()]

for filename in argv[1:]:
	name = path.basename(filename).replace('.csv','')
	## read data
	print("Opening file " + filename + "...")
	dataframe=read_csv(filename, sep=',',header=0)
	data = dataframe.values
	data_x = np.array(data[:,:-1])
	data_y = data[:,-1]
	## variance for normalizing MSEs
	variance = np.var(data_y)
	print("variance: " + str(variance))
	for algo in algorithms:
		algo.run()
		yerr = data_y - algo.yhat
		algo.mse = np.mean(yerr**2)/variance
		print("MSE: " + str(algo.mse))
		print("-"*20)
		np.savetxt(resultsdir+timestring+'_'+name+'_'+algo.name +'.csv', algo.yhat, delimiter=",")

	## write line to report
	report.write(name)
	for algo in algorithms:
		report.write(', '+str(algo.time)+', '+str(algo.mse))
	report.write('\n')
	
	## plots
	if(data.shape[1] == 2):
		## 2D plot
		plt.plot(train_x, train_y, 'r+', label='train data')
		plt.plot(test_x, test_y, 'b+', label='test data')
		plt.plot(data_x, ypge, 'y', label='PGE')
		plt.plot(data_x, yffx, label='FFX')
		plt.plot(data_x, ygp, 'g', label='gplearn')
		plt.plot(data_x, yspline, 'c', label='spline')
	elif(data.shape[1] == 3):
		if '_regular' in name:
			## 3D wireframe
			fig = plt.figure()
			ax = fig.add_subplot(111, projection='3d')
			numx = 1 + min([i for i,x in
				enumerate(data_x[1:,0]) if x > data_x[i-1,0]])
			numy = 1 + min([i for i,x in
				enumerate(data_x[:-1,1]) if x > data_x[i+1,1]])
			X1 = np.reshape(data_x[:,0], (numx, numy))
			X2 = np.reshape(data_x[:,1], (numx, numy))
			Zdata = np.reshape(data_y, (numx, numy))
			Zpge = np.reshape(ypge, (numx, numy))
			Zffx = np.reshape(yffx, (numx, numy))
			Zgp = np.reshape(ygp, (numx, numy))
			Zspline = np.reshape(yspline, (numx, numy))
			ax.plot_wireframe(X1, X2, Zdata, rstride=1, cstride=1,
				label='data')
			ax.plot_wireframe(X1, X2, Zpge, rstride=1, cstride=1,
				colors=['y'], label='PGE')
			ax.plot_wireframe(X1, X2, Zffx, rstride=1, cstride=1,
				colors=['r'], label='FFX')
			ax.plot_wireframe(X1, X2, Zgp, rstride=1, cstride=1, 
				colors=['g'], label='gplearn')
			ax.plot_wireframe(X1, X2, Zspline, rstride=1,
				cstride=1, colors=['c'], label='spline')
		else:
			## 3D scatterplot
			fig = plt.figure()
			ax = fig.add_subplot(111, projection='3d')
			X1 = data_x[:,0]
			X2 = data_x[:,1]
			ax.scatter(X1, X2, data_y,			label='data')
			ax.scatter(X1, X2, ypge, c='y', label='PGE')
			ax.scatter(X1, X2, yffx, c='r', label='FFX')
			ax.scatter(X1, X2, ygp,	c='g', label='gplearn')
			ax.scatter(X1, X2, yspline,	c='c', label='spline')
	else:
		print("Data is not 2D or 3D, can't plot.")
	plt.legend()
	plt.tight_layout(pad=0.0)
	graph = 'graphs/'+name.replace('.csv', '_graph.png')
	plt.savefig(graph)
	plt.clf()
report.close()
