\documentclass{article} 

\usepackage{amsmath}
\usepackage[english]{babel}
\usepackage{amssymb}
\usepackage{amsxtra}
\usepackage{listings}
\usepackage{graphics}
\usepackage{pgfplots}
\usepackage[utf8]{inputenc}

\newcommand{\refchapter}[1]{Chapter~\ref{#1}}
\newcommand{\refsec}[1]{Section~\ref{#1}}
\newcommand{\refeqn}[1]{Equation~(\ref{#1})}
\newcommand{\reffig}[1]{Figure~\ref{#1}}
\title{
{\bf \scriptsize RHEINISCH-WESTF\"ALISCHE TECHNISCHE HOCHSCHULE AACHEN \\
LuFG Informatik 12 (Prof. Dr. Uwe Naumann)} \vspace{2cm} \\
Examining Various Symbolic Regression Algorithms as Tools for Function and Curve Fitting}
\author{
Wanja Hentze (Matr.-Nr. 335148)
}

\date{\includegraphics[height=2cm]{figures/logo_stce}}

\begin{document}

\lstloadlanguages{[ISO]C++}
\lstset{basicstyle=\small, numbers=left, numberstyle=\footnotesize,
  stepnumber=1, numbersep=5pt, breaklines=true, escapeinside={/*@}{@*/}}


\pagestyle{headings}

\maketitle
\newpage
\section*{Erklärung}

Hiermit versichere ich, dass ich die Arbeit selbständig verfasst und keine
anderen als die angegebenen Quellen und Hilfsmittel benutzt sowie Zitate 
kenntlich gemacht habe. \\
\\
\\
\\
\\
\\
\\
\\
Wanja Hentze \\
\\
Aachen, den 26.6.2016.

\newpage
\tableofcontents
\newpage

\section{Abstract}
\label{ch:1}

Curve fitting and surface fitting problems arise in any area of research where it is useful to succintly summarize the patterns and relationships in large datasets through a mathematical function.

Symbolic Regression is a relatively novel method of solving function fitting problems algorithmically without constraining the solution to a specific syntactical form. In theory, a Symbolic Regression library should be able to describe all datasets obeying a relationship that is expressible in the elementary terms known to it.

We present a tool for synthesizing regression problems and benchmark three SR implementations as well as a traditional spline-based fitting method against 40 of these problems. We argue from these results that these SR libraries are not up to par with state-of-the-art fitting techniques in terms of precision nor speed, but are useful when terse formulas are needed.

\newpage
\section{Introduction}
\label{ch:2}

\subsection{Problem Statement}

In the classic curve fitting problem, one is presented with a set of $n$ data points $(x_n, y_n)$ that admit some relationship. The task is then to find a curve, i.e. a function $f$ that closely approximates this data. Specifically, an accurate solution to the problem should minimize some norm of the error vector $\epsilon_n = | f(x_n) - y_n |$. Usually, the mean squared error norm is chosen, but the max and sum norms are also appropriate for some applications.

The surface fitting problem is the extension of curve fitting to 3-dimension data. That is, given three-dimensional points $(x_n, y_n, z_n)$, the aim is to find a  $f$ minimizing $\epsilon_n = | f(x_n, y_n) - y_n |$.

Generalizing to the n-dimensional manifold fitting problem is an obvious next step, but this is outside of the scope of this survey.

\subsection{Symbolic Regression}

Usually, when using software to fit curves and surfaces to data, one must first hypothesize the nature of the relationship the data admit by providing a \textbf{model function} $f(x:c)$ depending on both the input data $x$ and a number of parameters $c$. The curve fitting algorithm will then try to find values of $c$ that minimize the error in $f(x:c)$ while leaving the syntax of the function term itself untouched. This yields accurate results when the model function is chosen appropriately for the data but is also naturally prone to human error in misinterpreting data.

Symbolic Regression is a different approach altogether, as the algorithm does not require a model function to find a relationship in data. Instead, it constructs many different models symbolically from a pool of base symbols, evaluates them on the test data and eventually returns the best fitting one among them.\\
Traditionally, Symbolic Regression is implemented using Genetic Programming, in which the pool of functions is subjected to natural selection (selecting for fitness) and then re-filled using the offspring from the best-fitting models.
However, not this is not the only way to approach this problem, as we will see in the case of FFX.\\

SR is desirable for researchers because it is completely agnostic to the nature of the data being analyzed and can therefore be applied to many very different datasets without needing fine-tuning. In the optimal case, SR will yield very concise function terms, making it possible to summarise relationships more efficiently than some traditional function fitting solutions such as splines. \\

A technique very similar to SR, then named "Symbolic Analysis" was originally employed in 1989 in the design of the analog circuit analyzer ISAAC. ISAAC was capable of deriving analytic functions describing the relationships between several parameters in any analog circuit.\cite{ISAAC}. The term "Symbolic Regression" first appeared in literature in \cite{Koza1992}, where it was studied as an application of Genetic Programming.\\
SR has recently received an increase in popularity after a publication in Science\cite{eureqapaper} and the subsequent release of the software "eureqa", a proprietary Symbolic Regression engine based on it which is marketed as a "Robotic Data Scientist". Rather recently, several open source projects have emerged with the goal of implementing SR in various languages. These projects include DataMelt\cite{DataMeltManual}, HeuristicLab\cite{heurconf}, PyPGE\cite{pgerepo}, gplearn\cite{gplearnrepo} and FFX\cite{ffxrepo}.\\

In the next section, the latter three of these are described briefly, along with their theoretical underlyings. Then, a new tool for producing arbitrary SR benchmarks is introduced. Finally, these benchmarks are run against all three of the libraries and the results are discussed.

\newpage
\section{Algorithms and Implementations}
\label{ch:3}
Any algorithm that solves regression problems symbolically, instead of being restrained to a single model or predefined set of models, is called a Symbolic Regression algorithm. The implementations of three of these algorithms were studied in this survey.

\subsection{GP-SR}
GP-SR is the application of Genetic Programming (GP) to Symbolic Regression.
GP-SR techniques are among the first Symbolic Regression algorithms.
In the usual setup, the initial generation is seeded with a number randomly generated function terms.
Every subsequent generation is then created by generating offspring from the best fitting functions of the last generation. Rules for creating offspring can vary but usually include $mutate$, where some subexpression is replaced with a different, randomly generated expression, $crossover$, where two subexpressions from two successful functions are exchanged with eachother.\cite{Zelinka2005}

The GP-SR implementation used in this study is the $genetic.SymbolicRegressor$ class from the gplearn Python library, found at \cite{gplearnrepo}.

\subsection{PGE}
PGE, or Prioritized Grammar Enumeration, is a recently developed algorithm based on GP-SR. Unlike regular GP-SR, PGE involves no randomness and will yield the same results deterministically. It uses dynamic programming techniques to reduce the size of the search space.\\

It works by generating all its models deterministically from a base pool, then using traditional least-squares regression to fit the parameters of these models to the data. For a complete description of PGE, see \cite{pgepaper}.\\

The PGE implementation studied in this survey is pypge, found at\cite{pgerepo}.
pypge is a Python 3 library written by the same author of the original paper about PGE, inspiring confidence that it is a faithful implementation. A Go library, $gopge$ is also available by the same author, but appears to not yet be in a working state\cite{gopgerepo}.\\

\subsection{FFX}
FFX, or Fast Function Extraction, is another Symbolic Regression Algorithm, first described in \cite{McConaghy2011}.

It works by generating all its models deterministically from a base pool, then using traditional least-squares regression to fit the parameters of these models to the data. Unlike in GP-SR, the values of these parameters are not considered part of the model, thereby reducing the search space.\\

The FFX implementation used in this study is Trent McConaghy's reference Python implementation found at \cite{ffxrepo}.

\section{Software written}

There exist a number of both synthetic and real-world datasets for benchmarking SR implementations. However, many of these are considered dissatisfactory by researchers\cite{gp-benchmarks-2012}. Concerns raised include that some benchmarks are too easy in general while other appear to be tailored to specific SR solutions.\\

Therefore, we present a Python tool called \textbf{modelgen} which generates random function terms from arbitrary symbols and then evaluates them to create datasets. The tool works by recursively building syntax trees from terminal symbols, unary operators and binary operators, until either a desired depth or a desired node count is reached. This is repeated until a function is found that is not indeterminate at any point in the desired input range and that is not constant in regard to any input variable.\\

\newpage
\section{Testing Setup}
\label{ch:4}
\subsection{Data}

For this survey, $modelgen$ was used to construct 20 one-dimensional function terms of complexity at most 8 and depth at most 5 and 20 two-dimensional functions of complexity at most 6 and depth at most 4. The allowed symbols used are listed in the following table:\\
$$
\begin{tabular}{|r|l|}
  \hline
  terminal & constant, variable, $\pi$, $\phi$, $e$, constant$\times$ variable \\ \cline{2-2}
  unary & sin, cos, sqrt, exp, $()^2$, ln, cosh \\ \cline{2-2}
  binary & +,-,$\times$,/ \\
  \hline
\end{tabular}
$$

Constants were chosen uniformly at random from $[-5,5]$.
A $constant \times variable$ terminal is assigned both a constant and a variable symbol. Semantically, this expression evaluates to the product of the constant's value and the variable's value. This terminal was included in order to facilitate the creation of functions react differently to a large change in a variable than to a small change.\\

Refer to the Appendix for a table of all the functions generated.\\ 

It should be noted that, while $modelgen$ will not create functions that are constant in any input variable, it does create functions that admit only a linear relationship between their output and one or even all input variables.
These functions are trivially fitted by most algorithms, but were still included in the survey in order to test the algorithms' best case runtimes.\\

The 1D functions were sampled 200 times, once along regularly spaced values from [-4,4] and once along points chosen uniformly at random from $[-4,4]$.
The 2D functions were sampled once on along a rectilinear 40x40 grid from $(-4,-4)$ to $(4,4)$ and once at 1600 points chosen uniformly at random from $[-4,4]^2$
\\

\subsection{Benchmarking Considerations}

For benchmarking the algorithms, a small wrapper called $evaluate.py$ was developed in order to run the sampled data through each of the libraries, capture their output in a unified format and report on their runtime as well as precision. \\

Part of the goal of this survey was to assess the utility of SR in comparison to traditional solutions. Therefore, spline interpolation methods from the industry standard scientific computing library SciPy were included in the benchmark. For the 1D samples, $interpolation.interp1d$ was used. For the 2D samples, $interpolation.RectBivariateSpline$ was used for the data sampled along a rectliniear grid and $interpolation.bisplrep$ was used for the data sampled at random points. As a spline created using every single data point would yield a perfect, yet very overfitted result, only part of the input data can be used for constructing the splines. In this benchmark, we use one in every ten points for $interp1d$ and one point out of every 4x4 subgrid for the 2D splines.

All tests were done using Debian GNU/Linux 8 on a quad-core, 3.4GHz Intel Haswell CPU with 2-way SMT and 12GB of memory. SciPy, FFC and gplearn were executed using CPython 2.7.12 while pgepy was executed using CPython 3.5.2.

gplearn was initialized using the following parameters:\\
$$
\begin{tabular}{|r|l|}
\hline
generations   & 20    \\ \cline{2-2}
n\_jobs       & 8     \\ \cline{2-2}
transformer   & True  \\ \cline{2-2}
trigonometric & True  \\ \cline{2-2}
metric        & 'mse' \\ 
\hline
\end{tabular}
$$\\
All other parameters remained at the default.\\

pypge was run with the $09\_final/config\_explicit.yml$ configuration from the pypge repository, with the number of workers increased to 8 to utilize all threads, remote hosts disabled and $abs$ added to the available functions to accomodate sharp bends in the data. Also, the iteration count was halved to 6 as iterations beyond that mark proved to yield little improvement but take very long to complete.\\

When running FFX, the input was split alternatingly into training and validation data.\\

For the sake of consistency, the error metric optimized for by each algorithm was Mean Squared Error($mse$). Although this metric allows us to compare the accuracy of different algorithms on the same dataset, its values vary vildly across the different samples generated by $modelgen$, as the functions' values can have very different magnitudes, absolute values sometimes going up to $10^6$ and sometimes not exceeding $10^{-6}$. This is intended as the algorithms are expected to perform well regardless of the magnitude of input values. However, for final results, it is favorable to have the error metrics be comparable across all samples. Therefore, all resulting $mse$s were normalized by dividing by the variance of the sample.\\

As pypge and gplearn use multithreading, while $scipy.interpolation$ and FFX do not, it is hard to fairly compare CPU time between them. Instead, the simple wall-clock time elapsed between the start and the end of each fitting procedure was taken as a measure of runtime. This also more closely resembles the real-world needs of researchers.\\


\section{Results}
\label{ch:5}

These are the results of the benchmark run:\\
\begin{tikzpicture}
\centering
\shorthandoff{}
\begin{axis}[
  legend pos=outer north east,
  xmin = 0.00001, xmax = 50000, xmode = log,
  ymin = 0.00000000001, ymax = 10, ymode = log,
  xlabel={time taken in seconds},
  ylabel={normalized MSE}]
\addplot[only marks, green] table [x=time(gplearn), y=mse(gplearn), col sep=comma] {goodreports/result.csv};
\addlegendentry{gplearn}
\addplot[only marks, red] table [x=time(ffx), y=mse(ffx), col sep=comma] {goodreports/result.csv};
\addlegendentry{ffx}
\addplot[only marks, yellow] table [x=time(pge), y=mse(pge), col sep=comma] {goodreports/result.csv};
\addlegendentry{pge}
\addplot[only marks, blue] table [x=time(spline), y=mse(spline), col sep=comma] {goodreports/result.csv};
\addlegendentry{spline}
\end{axis}
\end{tikzpicture}

For the full results, please refer to the Appendix.

Evidently, pypge was the slowest performing program by far, even after halving the default number of iterations und using 8 threads. However, this increase in runtime is also couple with more precision, in which pypge clearly outperforms gplearn and ffx.\\

FFX is the fastest SR program studied by a large margin. This falls in line with the authors' claims of very quick results. However, it is also yielded the least precise fits out of all the programs.\\

In comparison to the $spline$ results, the $SR$ results seem quite sobering.
When compared to the spline fitting algorithms, every FFX, the fastest SR algorithm, took over 10 times longer.

In terms of precision, the programs fall behing spline interpolation as well.
With an average mse of 0.1723(gp), 0.1025(pge) and 0.1317(ffx), none of them came close to the average mse of 0.007926 achieved using splines.

For an understanding why some of these libraries fail to capture a relationship in some cases, we should look at the cases that each respective library performed the worst at.

In the case of gplearn, the worst normalized $mse$ (1.00438) resulted from trying to fit $2D\_19\_irregular$.

Looking at the graph in Figure 1, it is easy to see why this is: the generated model is simply a constant term. For some reason or another, the algorithm was unable to react to the input at all. It is likely that the high complexity of these algorithms brings with it a possibility to fail completely.

\begin{figure}
\label{gpfail}
\caption{gplearn fails spectacularly at one example}
\includegraphics[width=0.8\textwidth]{figures/gpfail}
\end{figure}

FFX and PGE performed worst when tryin to fit $3D\_09\_regular$ and $3D\_09\_irregular$, respectively. This is likely because the underlying model here was $f(x,y) = cos(cosh(((-4.45210643*y) - ( -0.752746482 + (0.968456417*x)))))$, which is extremely sensitive in y, changing sign many hundred times in the testing interval.

However, there were also functions on which every single SR implementation performed badly that weren't as sensitive. For example, $3D\_regular\_02$. As seen in Figure 2, none of the SR fits were even close to the original surface, despite it being quite smooth.

\begin{figure}
\label{allfail}
\caption{all the SR implementations miss the data}
\includegraphics[width=0.8\textwidth]{figures/allfail}
\end{figure}

\section{Conclusion}
\label{ch:6}

While the SR libraries did yield decent results on most examples and in some cases were even able to recover the original function term entirely, their unpredictable behavior stops the author from recommending them as a 'one-size-fits-all'-solution to data analysis. For cases when a very simple model is needed but precision can be sacrificed, an ordinary low-order polynomial fit might be more useful. In cases when high precision is desired but the models need not neccesarily be very concise, splines are clearly superior.\\

This lands SR in an awkward position; too imprecise to beat splines, too complex to be preferrable over traditional regression analysis and too slow to be the first-line approach before trying everything else.

A possible exception is FFX, whose relatively quick runtimes make it an option to just throw it at any dataset one encounters. However, caution still needs to be taken to verify that the resulting model adequately describes the data.
The utopia of a single library that one can 'fire and forget' at any dataset whatsoever without any fine-tuning remains a utopia for now.


\nocite{}
\bibliographystyle{plain}
\bibliography{report}

\newpage
\appendix

\section{Source Code}
\label{app4}
This is the source code for $modelgen$ written for this paper.
For the up-to-date version, as well as the utility script evaluate.py and runpge.py please refer to \cite{modelgen}

\begin{lstlisting}
# -*- coding: utf-8 -*-

'''
modelgen.py by Wanja Hentze

To the extent possible under law, the person who associated CC0 with
modelgen.py has waived all copyright and related or neighboring rights
to modelgen.py.

You should have received a copy of the CC0 legalcode along with this
work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
'''

from math import *
from random import seed, random, randint, choice, uniform, gauss
from numpy import mean

def isbad(f):
	return (isnan(f) or isinf(f))

class expression:
	def evaluate(x):
		raise("wat")

## binary expressions
class bin_exp(expression):
	def __init__(self, complexity, depth, dim, symbols, varnames):
		self.varnames = varnames
		self.dim = dim
		leftcomp   = randint(0, complexity-2)
		rightcomp  = complexity-2 - leftcomp 
		self.left  = gen_exp(leftcomp,  depth-1, dim, symbols, varnames)
		self.right = gen_exp(rightcomp, depth-1, dim, symbols, varnames)
		self.op = choice(symbols.bin_ops)()

	def evaluate(self,values):
		return self.op.apply(self.left.evaluate(values), self.right.evaluate(values))
	
	def pr(self):
		return "(" + self.left.pr() + " " + self.op.pr() + " " + self.right.pr() + ")"
	
	def isconst(self):
		return self.left.isconst() and self.right.isconst()
	
	def constreplace(self):
		if(self.left.isconst() and not isinstance(self.left.op, namedconstant)):
			val = self.left.evaluate({x:0.0 for x in self.varnames})
			self.left = term_exp(symboltable, self.varnames)
			self.left.op = constant(val)
		else:
			self.left.constreplace()
		
		if(self.right.isconst() and not isinstance(self.right.op, namedconstant)):
			val = self.right.evaluate({x:0.0 for x in self.varnames})
			self.right = term_exp(symboltable, self.varnames)
			self.right.op = constant(val)
		else:
			self.right.constreplace()

class plus_op():
	def apply(self, x, y):
		return x + y

	def pr(self):
		return "+"

class mult_op():
	def apply(self, x, y):
		return x * y
	
	def pr(self):
		return "*"

class minus_op():
	def apply(self, x, y):
		return x - y
	
	def pr(self):
		return "-"

class div_op():
	def apply(self, x, y):
		if (y == 0):
			return x * float('NaN')
		return x/y
	
	def pr(self):
		return "/"


## unary expression
class un_exp(expression):
	def __init__(self, complexity, depth, dim, symbols, varnames):
		self.varnames=varnames
		self.dim=dim
		self.arg = gen_exp(complexity - 1, depth - 1, dim, symbols, varnames)
		self.op = choice(symbols.un_ops)()
	
	def evaluate(self, values):
		return self.op.apply(self.arg.evaluate(values))
	
	def pr(self):
		return self.op.pr1() + self.arg.pr() + self.op.pr2()

	def isconst(self):
		return self.arg.isconst()
	
	def constreplace(self):
		if(self.arg.isconst() and not isinstance(self.arg.op, namedconstant)):
			val = self.arg.evaluate({x:0.0 for x in varnames})
			self.arg = term_exp(symboltable)
			self.arg.op = constant(val)
		else:
			self.arg.constreplace()

class sin_op():
	def apply(self, x):
		if isbad(x):
			return float('NaN')
		return sin(x)
	
	def pr1(self):
		return "sin("
	def pr2(self):
		return ")"

class cos_op():
	def apply(self, x):
		if isbad(x):
			return float('NaN')
		return cos(x)
	
	def pr1(self):
		return "cos("
	def pr2(self):
		return ")"

class sqrt_op():
	def apply(self, x):
		if not (x > 0):
			return float('NaN')
		return sqrt(x)
	
	def pr1(self):
		return "√"
	def pr2(self):
		return ""

class exp_op():
	def apply(self, x):
		if isbad(x) or (x > 709.5):
			return(float('NaN'))
		return exp(x)
	
	def pr1(self):
		return "exp("
	def pr2(self):
		return ")"

class square_op():
	def apply(self, x):
		return x*x
	
	def pr1(self):
		return ""
	def pr2(self):
		return "^2"

class log_op():
	def apply(self, x):
		if (x <= 0):
			return float('NaN')
		return log(x)
	
	def pr1(self):
		return "ln("
	def pr2(self):
		return ")"

class acos_op():
	def apply(self, x):
		return acos(x)
	
	def pr1(self):
		return "acos("
	def pr2(self):
		return ")"

class abs_op():
	def apply(self, x):
		return abs(x)
	
	def pr1(self):
		return "|"
	def pr2(self):
		return "|"

class cosh_op():
	def apply(self, x):
		if isbad(x) or (abs(x) > 710.4):
			return float('NaN')
		return cosh(x)
	
	def pr1(self):
		return "cosh("
	def pr2(self):
		return ")"

class neg_op():
	def apply(self, x):
		if(isbad(x)):
			return float('NaN')
		return -x
	
	def pr1(self):
		return "-"
	def pr2(self):
		return ""

## terminal expressions
class term_exp(expression):
	def __init__(self, symbols, varnames):
		self.dim=len(varnames)
		self.op = choice(symbols.term_ops)(varnames)

	def evaluate(self, values):
		return self.op.apply(values)
	
	def pr(self):
		return self.op.pr()

	def isconst(self):
		return self.op.isconst()
	
	def constreplace(self):
		return

class term_op():
	pass

class variable(term_op):
	def __init__(self, varnames):
		self.symbol = choice(varnames)
	
	def apply(self, values):
		return values[self.symbol]
	
	def pr(self):
		return self.symbol
	
	def isconst(self):
		return False

class constant(term_op):
	def __init__(self, value):
		self.value = value
	
	def apply(self, x):
		return self.value
	
	def pr(self):
		return " " + str(self.value)
	
	def isconst(self):
		return True

def randomconstant(varnames):
	return constant(uniform(-5.0, 5.0))

namedconstants = [
	("e", e),
	("\pi", pi),
	("\phi", (1+sqrt(5))/2),
	("2", 2)
]

class namedconstant(term_op):
	def __init__(self, varnames):
		self.name, self.value = choice(namedconstants)
	
	def apply(self, values):
		return self.value
	
	def pr(self):
		return self.name
	
	def isconst(self):
		return True

class mult_variable(term_op):
	def __init__(self, varnames):
		self.varname = choice(varnames)
		self.mult = uniform(-5.0, 5.0)
	
	def apply(self, values):
		return self.mult * values[self.varname]
	
	def pr(self):
		return "(" + str(self.mult) + '*' + self.varname + ")"
	
	def isconst(self):
		return False

## default list of symbols
class symboltable:
	bin_ops = [plus_op, mult_op, minus_op, div_op]
	un_ops  = [sin_op, cos_op, sqrt_op, exp_op, abs_op, square_op, log_op, cosh_op]
	term_ops= [randomconstant, namedconstant, variable, variable, mult_variable]

## generate an expression with maximum complexity and depth, using symbols from symbol table
def gen_exp(complexity, depth, dim, symbols, varnames):
	if (complexity == 0 or depth == 0):
		return(term_exp(symbols, varnames))
	
	if (complexity == 1 or randint(0, 2) == 0):
		return(un_exp(complexity, depth, dim, symbols, varnames))
	else:
		return(bin_exp(complexity, depth, dim, symbols, varnames))

## evaluate exp num_samples times in an interval from xmin to xmax
def sample_exp(exp, num_samples=100, xmin=-4.0, xmax=4.0, regular=True):
	samples = list(dict())
	if(exp.dim == 1):
		if (regular):
			samples = [{exp.varnames[0]:xmin + ((xmax-xmin)/num_samples * i)}
				for i in range (num_samples)]
		else:
			samples = sorted([{exp.varnames[0]:uniform(xmin, xmax)}
				for i in range(num_samples)])
	elif(exp.dim == 2):
		if (regular):
			num1 = int(sqrt(num_samples))
			num2 = int(num_samples/num1)
			samples = [
				{exp.varnames[0]:xmin+((xmax-xmin)/num1 * x),
				 exp.varnames[1]:xmin+((xmax-xmin)/num2 * y)}
				for x in range(num1) for y in range(num2)]
		else:
			samples = [{exp.varnames[0]:uniform(xmin,xmax),
						exp.varnames[1]:uniform(xmin,xmax)}
						for i in range(num_samples)]
	
	return [(x, exp.evaluate(x)) for x in samples]

def addnoise(data, noise_type='additive', noise_amount=0.1):
	for tup in data:
		if noise_type == 'additive':
			tup[1] += uniform(-noise_amount, noise_amount)
		elif noise_type == 'multiplicative':
			tup[1] *= 1 + uniform(-noise_amount, noise_amount)
		elif noise_type == 'gaussian':
			tup[1] += gauss(0, noise_amount)
		
## repeatedly generate expressions until one yeilds no NaN, inf, constant or huge values
def modelgen(complexity, depth, dim = 1, symbols = symboltable(),
		varnames=['x'], constreplace = True):
	## only 1- and 2-dimensional functions supported for now
	assert(dim in [1,2])
	assert(len(varnames)==dim)
	
	while(1):
		exp = gen_exp(complexity, depth, dim, symbols, varnames)
		## try exp on some inputs to see if it's well-behaved
		## we only want non-constant, non-huge and well-defined models
		if(dim==1):
			sams = sample_exp(exp, 200)
			isconstant = (sams[0][1] == sams[10][1])
		else:
			sams = sample_exp(exp, 1600)
			isconstant = (sams[0][1] == sams[10][1] or
				sams[0][1] == sams[20][1])
		
		hugesample = (max([abs(s[1]) for s in sams]) > 100000)
		badsample = any([isbad(s[1]) for s in sams])
		if not (isconstant or hugesample or badsample):
			break
	if constreplace:
		exp.constreplace()
	return exp

def writearraytoffx(data, basename):
	fin  = open(basename + '_in.csv', 'w')
	fout = open(basename + '_out.csv', 'w')
	for tup in data[:-1]:
		fin.write(str(tup[0])+'\n')
		fout.write(str(tup[1])+' ')
	## no trailing space after last entry
	fin.write(str(data[-1][0]))
	fout.write(str(data[-1][1]))
	fin.close
	fout.close

## representation structure for sample data
class data:
	def __init__(self, samples, expression, name):
		self.samples = samples
		self.expression = expression
		self.name = name
	
	## simple, single csv file output
	def writetocsv(self, filename, writenames=False):
		f = open(filename, 'w')
		varnames = sorted(self.samples[0][0].keys())
		if(writenames):
			f.write(', '.join(varnames) + ', f\n')
		for tup in self.samples:
			ins = tup[0]
			out = tup[1]
			f.write(', '.join([str(ins[x]) for x in varnames]))
			f.write(', {0:12f}\n'.format(out))
		f.close
	
	## write to 4 separate files as required by ffx,
	## separating training/testing data and input/output
	def writeforffx(self, basename, testportion=0.25):
		cutoff = int(testportion*len(self.samples))
		traindata = self.samples[:cutoff]
		testdata  = self.samples[cutoff:]
		writearraytoffx(traindata, basename+'_train')
		writearraytoffx(testdata,  basename+'_test')

## print a list of 2-tuples csv-style
def printcsv(res):
	s = ''
	for tup in res:
		s += '{0:10f}, {1:20f}\n'.format(tup[0], tup[1])
	return s
\end{lstlisting}

\newpage
\section{Tables}
\begin{figure}
\caption{Functions generated by modelgen for the benchmark}

\begin{tabular}{r|l}
\label{functable}
name  & function \\ \hline
2D\\ \hline
2D\_01 & f(x) = $(x^2 + (|x| * ( -4.56512709643 - x)))$ \\ \hline
2D\_02 & f(x) = $(|cosh(((x * x) / \phi))| + x)$ \\ \hline
2D\_03 & f(x) = $((\phi - x) * (( 3.60637533116 - (0.691075034743*x)) - 2))$ \\ \hline
2D\_04 & f(x) = $( 0.909297426826 * (x *  -1.41101209934)^2)$ \\ \hline
2D\_05 & f(x) = $((( 1.05967433178 + x) - x) * cos(x))$ \\ \hline
2D\_06 & f(x) = $(( 5.09462279118 - (x - (-0.859935588347*x))) *  -0.62333821953)$ \\ \hline
2D\_07 & f(x) = $(((-3.55744916643*x) /  -1.91518175898) *  51.3824862983)$ \\ \hline
2D\_08 & f(x) = $exp((x / exp((x *  0.647042331227))))$ \\ \hline
2D\_09 & f(x) = $(sin(cosh((0.0278208005221*x))) + ( 1.46362561206 + cos(x)))$ \\ \hline
2D\_10 & f(x) = $(((1.53472533901*x) +  0.454635674358) *  0.460017719273)$ \\ \hline
2D\_11 & f(x) = $((x /  0.0141815683828) /  1.01322769574)$ \\ \hline
2D\_12 & f(x) = $(( -1.25245507937 + |(1.01457038*x)|^2) * ((3.18820127*x) *  1.86945504))$ \\ \hline
2D\_13 & f(x) = $\surd((e * cos(|x|)) + |x|)$ \\ \hline
2D\_14 & f(x) = $cos(((\phi * x^2) -  0.636619772368))$ \\ \hline
2D\_15 & f(x) = $(sin(x) - (x^2 / ((4.4502480953*x) +  -3.59681877986)))$ \\ \hline
2D\_16 & f(x) = $((\pi / cosh(cos(x))) + x)^2$ \\ \hline
2D\_17 & f(x) = $((x^2 -  1.15828895456) *  3.76219569108)$ \\ \hline
2D\_18 & f(x) = $cos((sin(exp(exp(x))) + (x + (0.448628922502*x))))$ \\ \hline
2D\_19 & f(x) = $( 14.079641459 + sin(x))$ \\ \hline
2D\_20 & f(x) = $(((-4.18534016562*x) + x) + cos((x /  22.7200248932)))$ \\ \hline
3D \\ \hline
3D\_01 & f(x, y) = $(exp((y /  -4.71652523478)) * x)^2$ \\ \hline
3D\_02 & f(x, y) = $(exp(cos((x - y))) / \phi)$ \\ \hline
3D\_03 & f(x, y) = $(cosh(y) - (cos((1.71273542163*y)) - (4.04695984512*y)))$ \\ \hline
3D\_04 & f(x, y) = $((\phi * ((1.0585188372*x) + y)) /  4.345883639)$ \\ \hline
3D\_05 & f(x, y) = $((sin(y) - y) - cos((-0.309309522178*x)))$ \\ \hline
3D\_06 & f(x, y) = $( 0.0 + y)$ \\ \hline
3D\_07 & f(x, y) = $(cosh(y) - ((-0.818771782148*x) + exp(x)))$ \\ \hline
3D\_08 & f(x, y) = $( 0.137766955316 + (y / \pi))$ \\ \hline
3D\_09 & f(x, y) = $cos(cosh(((-4.45210643*y) - ( -0.752746482 + (0.968456417*x)))))$ \\ \hline
3D\_10 & f(x, y) = $(( -0.586531825464 + (-4.64682170212*x)) * ( -3.91243587373 * y))$ \\ \hline
3D\_11 & f(x, y) = $((2 * (2.07809621498*y)) /  2.71828182846)$ \\ \hline
3D\_12 & f(x, y) = $( -5.25245507937 * cos(y))$ \\ \hline
3D\_13 & f(x, y) = $(exp(y) *  1.0)$ \\ \hline
3D\_14 & f(x, y) = $(|(-3.50296443991*y)| * (x /  -0.911733914787))$ \\ \hline
3D\_15 & f(x, y) = $(( -3.27805492177 * (2.19082340728*y)) -  12.2462568664)$ \\ \hline
3D\_16 & f(x, y) = $((y + \phi) /  2.71828182846)$ \\ \hline
3D\_17 & f(x, y) = $ln((cosh(((0.399633506201*x) + (-1.90800470042*y))) + y))$ \\ \hline
3D\_18 & f(x, y) = $((e + y) * ( -2.93162236756 * x))$ \\ \hline
3D\_19 & f(x, y) = $exp(sin((y - x)^2))$ \\ \hline
3D\_20 & f(x, y) = $(( 2.62072689811 * (-3.30621912874*y)) * cosh((-1.80860851151*x)))$ \\ \hline
\end{tabular}
\end{figure}

\end{document}

