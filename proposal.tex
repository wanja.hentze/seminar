\documentclass{article} 

\usepackage{amsmath}
\usepackage[english]{babel}
\usepackage{amssymb}
\usepackage{amsxtra}
\usepackage{listings}
\usepackage{graphics}
\usepackage{pgfplots}

\newcommand{\refchapter}[1]{Kapitel~\ref{#1}}
\newcommand{\refsec}[1]{Sektion~\ref{#1}}
\newcommand{\refeqn}[1]{Gleichung~(\ref{#1})}
\newcommand{\reffig}[1]{Abbildung~\ref{#1}}

\title{
{\bf \scriptsize RHEINISCH-WESTF\"ALISCHE TECHNISCHE HOCHSCHULE AACHEN \\
LuFG Informatik 12 (Prof. Dr. Uwe Naumann)} \vspace{2cm} \\
Examining Symbolic Regression as a General Approach to Function and Curve Fitting \\
{\large Research Proposal} }
\author{
Wanja Hentze (Matr.-Nr. 335148)
}

%\date{\includegraphics[height=2cm]{figures/logo_stce}}

\begin{document}

\lstloadlanguages{[ISO]C++}
\lstset{basicstyle=\small, numbers=left, numberstyle=\footnotesize,
  stepnumber=1, numbersep=5pt, breaklines=true, escapeinside={/*@}{@*/}}


\pagestyle{headings}

\maketitle
\newpage

\section*{Erkl\"arung}

Hiermit versichere ich, dass ich die Arbeit selbst\"andig verfasst und keine
anderen als die angegebenen Quellen und Hilfsmittel benutzt sowie Zitate 
kenntlich gemacht habe. \\
\\
\\
\\
\\
\\
\\
\\
Wanja Hentze \\
\\
Aachen, den 9.4.2016.

\newpage
\tableofcontents
\newpage

\section{Abstract}
\label{ch:2}

Curve fitting and surface fitting problems arise in any area of research where it is useful to succintly summarize the patterns and relationships in large datasets through a mathematical function.

In this study, I want to take a investigate a new way of looking at these problems, which is symbolic regression, and to evaluate how well implementations of it perform when compared to traditional numerical libraries.

Most pieces of curve or surface fitting software require a great deal of forethought by the user, as there has to be a hypothesis about the patterns in the data in advance in order to fit a curve or surface to it.
Symbolic Regression provides a much more intelligent, "fire-and-forget" type of solution based in genetic programming that can adapt to nearly any kind of data with little or even no human interaction. However, since it has to search a much wider problem space, it is also usually several orders of magnitude more expensive when it comes to computational effort. My goal for this research project will be to compare the accuracy, usability and efficiency of some state-of-the-art symbolic regression libraries and to evaluate if this hike in computational complexity is a worthy trade-off.

\section{Scope of Research}
\label{ch:3}

\subsection{Problem Statement}

In the classic curve fitting problem, one is presented with a set of $n$ data points $(x_n, y_n)$ that admit some relationship. The task is then to find a curve, i.e. a function $f$ that closely approximates this data. Specifically, an accurate solution to the problem should minimize some norm of the error vector $\epsilon_n = | f(x_n) - y_n |$. Usually, the sum-of-squares norm is chosen, but the max and sum norms are also appropriate for some applications.

The surface fitting problem is essentially similar but is concerned with fitting a function that takes 2-dimensional input to a data set consisting of three-dimensional points $(x_n, y_n, z_n)$, i.e.  $f: X \times Y \rightarrow Z$ . Fitting manifolds of arbitrary dimension (hypersurfaces) are also possible, but this study will focus on the 2- and 3-dimensional cases.

\subsection{Symbolic Regression}

Usually, when using software to fit curves and surfaces to data, one must first hypothesize the nature of the relationship the data admit by providing a \textbf{model function} $f(x:c)$ depending on both the input data $x$ and a number of parameters $c$. The curve fitting software will then try to find values of $c$ that minimize the error in $f(x:c)$ while leaving the syntax of the function term itself untouched. This yields accurate results when the model function is chosen appropriately for the data but is also naturally prone to human error in misinterpreting data. For example, a researcher could hypothesize that their dataset admits a quadratic relationship and use $f(x) = c_1x^2 + c_2x + c_3$ as their model function when the true underlying curve is that of a hyperbolic cosine, thus yielding both a mediocre fit and an incorrect hypothesis about the nature of their data, as seen in \ref{fig:cosh}.

Symbolic regression is a different approach altogether, as the software does not require a model function to find a relationship in data. Instead, techniques from genetic programming such as the ones detailed in \cite{genconf} are used to constructs a fitting function term from scratch. A pool of candidate functions is populated with random atomic functions like constants and variable symbols. Then, the best fits among these are mutated and combined syntactically to form the next iteration of the candidate pool. This process is iterated many times, often several thousand, until a close fit is obtained. An immediate drawback becomes obvious: due to the many iterations required, this is a very computationally expensive method of function fitting.
On the upside, this approach is completely agnostic of the nature of the data and can be used without human supervision on a highly heterogenous array of datasets.

\cite{SRpaper} provides a more detailed explanation of the symbolic regression procedure.

\begin{figure}
\centering
\begin{tikzpicture}
\begin{axis}[
xlabel=$x_n$,
ylabel=$y_n$,
]

\addplot[
	only marks,
] coordinates {
(1.96369711594, 4.2886592862)
(3.38323051321, 16.2829053535)
(1.40087787424, 2.50825323997)
(0.56737616313, 0.959010375341)
(-0.738771588863, 1.16706968622)
(3.7176841737, 21.7339729606)
(2.84975242944, 8.3387684708)
(2.30478012541, 5.58057187041)
(0.0942244453988, 1.11439716398)
(2.99175643944, 9.27891088525)
(-3.65837909772, 20.3308340349)
(0.978039753299, 1.65480433615)
(-3.8924446554, 27.3043869935)
(-0.429313706692, 1.21104970519)
(-2.0758607305, 4.24818843718)
(-1.93307495238, 3.04403471424)
(-3.21572104149, 12.6235920373)
(0.0547424036229, 1.20067395147)
(2.19888833336, 4.7006676355)
(2.36835361259, 6.16301715445)
(-1.4310712257, 1.88610581254)
(-0.499570760418, 1.04401535778)
(-3.56165358583, 14.8943876381)
(1.88008635589, 2.80903276149)
(-2.12452969597, 4.21263502064)
(-1.61226253991, 2.92665853584)
(3.93644534665, 21.5326071811)
(1.14250978281, 2.03276139953)
(3.10375173714, 13.1400085787)
(0.399593318024, 1.1258070837)
(2.64138893173, 8.06493723457)
(1.93147365925, 4.02434345938)
(-0.976575498101, 1.61630177481)
(-2.3726157536, 6.12642924294)
(-3.62848311956, 15.6765786652)
(3.79441717157, 17.8801549605)
(-0.672634443375, 1.37747652883)
(0.318362887219, 1.20034748198)
(1.65758298854, 2.99858010536)
(-3.18921851075, 14.0104406275)
(3.53140809836, 19.2584030266)
(2.65997260934, 6.16046861453)
(-0.519868274342, 1.15417892854)
(-2.75856795963, 6.3615397718)
};
	\addlegendentry{data}

\addplot[
	blue,
	domain=-4:4
	] {cosh(x)};
	\addlegendentry{cosh(x)}

\addplot[
	red,
	domain=-4:4
	] {2*x^2 - 2};
	\addlegendentry{f(x)}

\end{axis}
\end{tikzpicture}

\caption{Some data admitting the relationship $y \approx cosh(x)$ are fitted using a model function to obtain the best quadratic fit $f(x)$}
\label{fig:cosh}
\end{figure}

\subsection{Candidate Libraries}

Several recently emerging function fitting tools make use of Symbolic Regression. This survey will focus on a few of them in particular, Eureqa, HeuristicLab and DataMelt.

\subsubsection{Eureqa}
\label{sec:3.3.1}

Eureqa is a recently emerged, commercial program for data analysis. It was one of the first to make use of symbolic regression and its authors demonstrate some promising results in \cite{eureqapaper}.

\subsubsection{HeuristicLab}
\label{sec:3.3.3}
HeuristicLab is an open-source framework that implements many advanced modelling algorithms including symbolic regression using genetic programming for not only finding a fitting function but also simplifying a given function to distill relationships down to their essential parts.\cite{heurconf}.

\subsubsection{DataMelt}
\label{sec:3.3.3}
DataMelt is a free data-analysis framework written in Java that implements, amongst a gost of other functions, symbolic regression\cite{DataMeltManual}.

\subsubsection{NAG}
\label{sec:3.3.4}

NAG is a widely used commercial numerical library with a very wide selection of functions. It does not, at the moment, provide symbolic regression functionality. However, it implements several traditional solutions for curve and surface fitting \cite{NAGintro}. Therefore, it will serve as a point of reference to compare the other libraries against.

\section{Research Plan}
\label{ch:4}

The following is a short outline of the projected way research will progress.

\subsection{Possible Issues}
\label{sec:4.1}

Several things could complicate this study. First of all, to test the general-purpose viability of the libraries, a number of datasets with very different kinds of patterns is needed. Also, they need to be categorized so that the correct curve-fitting function can be chosen from NAG - although the symbolic regression libraries use a generic approach, this is not the case for NAG.

Another difficulty will be to adequately compare the results of these libraries, as their output formats are quite different. Most likely, a sort of testing suite will have to be developped to wrap the functionalities of the libraries.

\subsection{Roadmap}

The progress of research will be oriented around these milestones:
\begin{itemize}
\item April 15th - April 30th: Gather and preprocess sample data
\item April 15th - May 15th: Set up fitting software and testing infrastructure
\item May 1st - May 31st: Write a preliminary version of the report covering abstract, motivation and test setup
\item May 15th - May 31st: Gather results from the tests
\item June 1st - June 15th: Write the "Results" and "Conclusion" sections of the report
\item June 16th - July 3rd: Buffer period to gather feedback, fix issues and finalize the report
\end{itemize}

\nocite{}
\bibliographystyle{plain}
\bibliography{proposal}

\newpage
\appendix

\end{document}

